#!/usr/bin/env python3
"""
Pantomime API stub for development server.
"""
from json import dumps as json_dumps
from threading import Thread
from time import sleep

from aiohttp import web
from requests import post


routes = web.RouteTableDef()


def call_callback(id):
    sleep(5)
    data = json_dumps({
        "status": "ok",
        "id_result": id
    })
    post("http://127.0.0.1:5000/callback", data=data)


@routes.get("/")
async def index(request):
    """Index page."""
    return web.Response(text="Pantomime API stub.")


@routes.post("/animate")
async def animate(request):
    """Mime animate API stub."""
    return web.json_response({
        "status": "OK",
        "path": "http://127.0.0.1:8080/mime"
    })


@routes.get("/mime")
async def mime(request):
    """Return API mime."""
    return web.FileResponse("mime_yum_yum.gif")


@routes.post("/animate_video")
async def animate_video(request):
    """Animate video API stub."""
    thread = Thread(target=call_callback, args=("0123456789ABCDEF",))
    thread.start()
    return web.json_response({
        "task_id": "0123456789ABCDEF"
    })


@routes.get("/get_result/{id}")
async def get_result(request):
    """Returns API "result"."""
    return web.FileResponse("mime_yum_yum.mp4")


app = web.Application()
app.add_routes(routes)

if __name__ == "__main__":
    web.run_app(app)
